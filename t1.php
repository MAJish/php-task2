<?php

function findTemplateOccurrences($data, $template): array
{
    $resultItem = [];
    foreach ($template as $key => $value) {
        if (is_array($value)) {
              if (isset($data[$key])) {
                  $resultItem[$key] = findTemplateOccurrences($data[$key], $value);
            } else {
                $resultItem[$key] = $value;
            }
        } else {

            if (isset($data[$key])) {
                $resultItem[$key] = $data[$key];
            } else {
                $resultItem[$key] = $value;
            }
        }
    }

    return $resultItem;
}

function generateTotalStatsData(array $data): array
{
    // Создание массива для хранения общего количества продаж по категориям
    $categorySales = [];

    // Подсчет общего количества продаж по категориям
    foreach ($data as $item) {
        $categoryId = $item['body']['categoryId'];
        $purchasesTotal = $item['body']['purchasesTotal'];

        if (!isset($categorySales[$categoryId])) {
            $categorySales[$categoryId] = 0;
        }

        $categorySales[$categoryId] += $purchasesTotal;
    }

    // Нахождение категории с наибольшим количеством продаж
    $maxPurchases = 0;
    $topCategory = null;
    foreach ($categorySales as $categoryId => $purchases) {
        if ($purchases > $maxPurchases) {
            $maxPurchases = $purchases;
            $topCategory = $categoryId;
        }
    }

    // Поиск трех самых продаваемых товаров из категории с наибольшим количеством продаж
    $topProducts = [];
    foreach ($data as $item) {
        if ($item['body']['categoryId'] === $topCategory) {

            $productName = $item['body']['title'];
            $purchases = $item['body']['purchasesTotal'];

            $topProducts[] = [
                'productName' => $productName,
                'purchases' => $purchases
            ];
        }
    }
    usort($topProducts, function ($a, $b) {
        return $b['purchases'] - $a['purchases'];
    });
    $topProducts = array_slice($topProducts, 0, 3);

    // Формирование данных для total_stats.json
    return [
        'topCategory' => [
            'id' => $topCategory,
            'purchases' => $maxPurchases
        ],
        'topProducts' => $topProducts
    ];
}

$dataFile = 'data.json';
$data = json_decode(file_get_contents($dataFile), true, 512, JSON_UNESCAPED_UNICODE);

if (isset($_GET['format'])){
    $templateFile = null;
    if ($_GET['format'] == 'sku') {
        $templateFile = 'models/sku_tmpl.json';
    }
    elseif ($_GET['format'] == 'stats'){
        $templateFile = 'models/stats_tmpl.json';
        $results_stats = generateTotalStatsData($data);
        $resultsJson = json_encode($results_stats , JSON_UNESCAPED_UNICODE);
        file_put_contents('total_stats.json', $resultsJson);
    }
    elseif ($_GET['format'] == 'min'){
        $templateFile = 'models/min_tmpl.json';
    }
    if (isset($templateFile)){
        $template = json_decode(file_get_contents($templateFile), true);
        $results = [];
        foreach ($data as $item) {
            // Если значение шаблона является массивом, вызываем рекурсивно функцию для поиска вложенных вхождений
            $results[] = findTemplateOccurrences($item['body'], $template);
        }

        $resultsJson = json_encode($results, JSON_UNESCAPED_UNICODE);
        file_put_contents('tmpl_result.json', $resultsJson);

        echo "Файл tmpl_result.json успешно записан.\n";
    }
    else {
        echo "Заданного шаблона не найденно.\n";
    }
}
else{
    echo "Не переданы данные.\n";
}

